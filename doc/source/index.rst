Boartty
=======

Boartty is a console-based interface to the Storyboard task-tracking
system.

As compared to the web interface, the main advantages are:

 * Workflow -- the interface is designed to support a workflow similar
   to reading network news or mail.  In particular, it is designed to
   deal with a large number of stories across a large number of
   projects.

 * Offline Use -- Boartty syncs information about changes in
   subscribed projects to a local database.  All review operations are
   performed against that database and then synced back to Storyboard.

 * Speed -- user actions modify locally cached content and need not
   wait for server interaction.

Contents:

.. toctree::
   :maxdepth: 1

   installation.rst
   configuration.rst
   usage.rst
   contributing.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

