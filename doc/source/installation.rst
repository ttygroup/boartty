Installation
------------

Source
~~~~~~

When installing from source, it is recommended (but not required) to
install Boartty in a virtualenv.  To set one up::

  virtualenv boartty-env
  source boartty-env/bin/activate

To install the latest version from the cheeseshop::

  pip install boartty

To install from a git checkout::

  pip install .
