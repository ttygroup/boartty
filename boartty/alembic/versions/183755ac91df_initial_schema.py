"""initial schema

Revision ID: 183755ac91df
Revises: None
Create Date: 2016-10-31 08:54:59.399741

"""

# revision identifiers, used by Alembic.
revision = '183755ac91df'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('comment',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('parent_comment_key', sa.Integer(), nullable=True),
    sa.Column('content', sa.Text(), nullable=True),
    sa.Column('draft', sa.Boolean(), nullable=False),
    sa.Column('pending', sa.Boolean(), nullable=False),
    sa.Column('pending_delete', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['parent_comment_key'], ['comment.key'], ),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_comment_draft'), 'comment', ['draft'], unique=False)
    op.create_index(op.f('ix_comment_id'), 'comment', ['id'], unique=False)
    op.create_index(op.f('ix_comment_pending'), 'comment', ['pending'], unique=False)
    op.create_index(op.f('ix_comment_pending_delete'), 'comment', ['pending_delete'], unique=False)
    op.create_table('project',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.Column('subscribed', sa.Boolean(), nullable=True),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_project_id'), 'project', ['id'], unique=False)
    op.create_index(op.f('ix_project_name'), 'project', ['name'], unique=False)
    op.create_index(op.f('ix_project_subscribed'), 'project', ['subscribed'], unique=False)
    op.create_index(op.f('ix_project_updated'), 'project', ['updated'], unique=False)
    op.create_table('sync_query',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_sync_query_name'), 'sync_query', ['name'], unique=True)
    op.create_index(op.f('ix_sync_query_updated'), 'sync_query', ['updated'], unique=False)
    op.create_table('system',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_table('tag',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_tag_id'), 'tag', ['id'], unique=False)
    op.create_index(op.f('ix_tag_name'), 'tag', ['name'], unique=False)
    op.create_table('topic',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.Column('sequence', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_topic_name'), 'topic', ['name'], unique=False)
    op.create_index(op.f('ix_topic_sequence'), 'topic', ['sequence'], unique=True)
    op.create_table('user',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=255), nullable=True),
    sa.Column('email', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=False)
    op.create_index(op.f('ix_user_id'), 'user', ['id'], unique=False)
    op.create_index(op.f('ix_user_name'), 'user', ['name'], unique=False)
    op.create_table('project_topic',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('project_key', sa.Integer(), nullable=True),
    sa.Column('topic_key', sa.Integer(), nullable=True),
    sa.Column('sequence', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['project_key'], ['project.key'], ),
    sa.ForeignKeyConstraint(['topic_key'], ['topic.key'], ),
    sa.PrimaryKeyConstraint('key'),
    sa.UniqueConstraint('topic_key', 'sequence', name='topic_key_sequence_const')
    )
    op.create_index(op.f('ix_project_topic_project_key'), 'project_topic', ['project_key'], unique=False)
    op.create_index(op.f('ix_project_topic_topic_key'), 'project_topic', ['topic_key'], unique=False)
    op.create_table('story',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('user_key', sa.Integer(), nullable=True),
    sa.Column('status', sa.String(length=16), nullable=False),
    sa.Column('hidden', sa.Boolean(), nullable=False),
    sa.Column('subscribed', sa.Boolean(), nullable=False),
    sa.Column('title', sa.String(length=255), nullable=True),
    sa.Column('private', sa.Boolean(), nullable=False),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('last_seen', sa.DateTime(), nullable=True),
    sa.Column('outdated', sa.Boolean(), nullable=False),
    sa.Column('pending', sa.Boolean(), nullable=False),
    sa.Column('pending_delete', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['user_key'], ['user.key'], ),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_story_created'), 'story', ['created'], unique=False)
    op.create_index(op.f('ix_story_hidden'), 'story', ['hidden'], unique=False)
    op.create_index(op.f('ix_story_id'), 'story', ['id'], unique=False)
    op.create_index(op.f('ix_story_last_seen'), 'story', ['last_seen'], unique=False)
    op.create_index(op.f('ix_story_outdated'), 'story', ['outdated'], unique=False)
    op.create_index(op.f('ix_story_pending'), 'story', ['pending'], unique=False)
    op.create_index(op.f('ix_story_pending_delete'), 'story', ['pending_delete'], unique=False)
    op.create_index(op.f('ix_story_status'), 'story', ['status'], unique=False)
    op.create_index(op.f('ix_story_subscribed'), 'story', ['subscribed'], unique=False)
    op.create_index(op.f('ix_story_title'), 'story', ['title'], unique=False)
    op.create_index(op.f('ix_story_updated'), 'story', ['updated'], unique=False)
    op.create_index(op.f('ix_story_user_key'), 'story', ['user_key'], unique=False)
    op.create_table('event',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('type', sa.String(length=255), nullable=False),
    sa.Column('user_key', sa.Integer(), nullable=True),
    sa.Column('story_key', sa.Integer(), nullable=True),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('comment_key', sa.Integer(), nullable=True),
    sa.Column('info', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['comment_key'], ['comment.key'], ),
    sa.ForeignKeyConstraint(['story_key'], ['story.key'], ),
    sa.ForeignKeyConstraint(['user_key'], ['user.key'], ),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_event_created'), 'event', ['created'], unique=False)
    op.create_index(op.f('ix_event_id'), 'event', ['id'], unique=False)
    op.create_index(op.f('ix_event_type'), 'event', ['type'], unique=False)
    op.create_index(op.f('ix_event_user_key'), 'event', ['user_key'], unique=False)
    op.create_table('story_tag',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('story_key', sa.Integer(), nullable=True),
    sa.Column('tag_key', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['story_key'], ['story.key'], ),
    sa.ForeignKeyConstraint(['tag_key'], ['tag.key'], ),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_story_tag_story_key'), 'story_tag', ['story_key'], unique=False)
    op.create_index(op.f('ix_story_tag_tag_key'), 'story_tag', ['tag_key'], unique=False)
    op.create_table('task',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('title', sa.String(length=255), nullable=True),
    sa.Column('status', sa.String(length=16), nullable=True),
    sa.Column('creator_user_key', sa.Integer(), nullable=True),
    sa.Column('story_key', sa.Integer(), nullable=True),
    sa.Column('project_key', sa.Integer(), nullable=True),
    sa.Column('assignee_user_key', sa.Integer(), nullable=True),
    sa.Column('priority', sa.String(length=16), nullable=True),
    sa.Column('link', sa.Text(), nullable=True),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('pending', sa.Boolean(), nullable=False),
    sa.Column('pending_delete', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['assignee_user_key'], ['user.key'], ),
    sa.ForeignKeyConstraint(['creator_user_key'], ['user.key'], ),
    sa.ForeignKeyConstraint(['project_key'], ['project.key'], ),
    sa.ForeignKeyConstraint(['story_key'], ['story.key'], ),
    sa.PrimaryKeyConstraint('key')
    )
    op.create_index(op.f('ix_task_assignee_user_key'), 'task', ['assignee_user_key'], unique=False)
    op.create_index(op.f('ix_task_created'), 'task', ['created'], unique=False)
    op.create_index(op.f('ix_task_creator_user_key'), 'task', ['creator_user_key'], unique=False)
    op.create_index(op.f('ix_task_id'), 'task', ['id'], unique=False)
    op.create_index(op.f('ix_task_pending'), 'task', ['pending'], unique=False)
    op.create_index(op.f('ix_task_pending_delete'), 'task', ['pending_delete'], unique=False)
    op.create_index(op.f('ix_task_project_key'), 'task', ['project_key'], unique=False)
    op.create_index(op.f('ix_task_status'), 'task', ['status'], unique=False)
    op.create_index(op.f('ix_task_story_key'), 'task', ['story_key'], unique=False)
    op.create_index(op.f('ix_task_title'), 'task', ['title'], unique=False)
    op.create_index(op.f('ix_task_updated'), 'task', ['updated'], unique=False)
    ### end Alembic commands ###


def downgrade():
    pass
